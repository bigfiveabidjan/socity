import React from "react";
import '../App.css';
import {menu} from '../data';

function Menu() {
    return (
        <div className="menu">
            <ul>
                {menu.map((data, key) => {
                return (
                    <li key={key}>
                        <a href={data.link}>{data.text}</a>
                    </li>
                );
                })}
            </ul>
        </div>
    );
};


export default Menu;