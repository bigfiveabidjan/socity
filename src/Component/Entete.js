import React from "react";
import '../App.css';
import {enTete} from '../data';
import logo from '../img/logo.png';
import Menu from './Menu';

function Entete() {
    return (
        <div className="component-header">
            <div className="section">
                <div className="logo"><a href={enTete.site_url}><img src={logo} alt={enTete.logo_alt}/></a></div>
                <Menu />
            </div>
        </div>
    );
};


export default Entete;