import React from "react";
import '../App.css';
import {appLink} from '../data';

function BouttonTelechargement() {
    return (
        <div className="buton">
            {appLink.map((data, key) => {
            return (
                // eslint-disable-next-line
                <div key={key}>
                    <a href={data.link} ><img src={data.favicon} alt={data.text}/>{data.text}</a>
                </div>
            );
            })}
        </div>
    );
};


export default BouttonTelechargement;