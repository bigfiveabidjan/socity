import React from "react";
import '../App.css';
import {bonsPlans} from '../data';

function BonsPlans() {
    return (
        <div className="component-bonsplans">
            <div className="section">
                {bonsPlans.map((data, key) => {
                    return (
                        // eslint-disable-next-line
                        <div className={data.class} key={key}>
                            <img src={data.favicon} alt=""/>
                            <h2>{data.titre}</h2>
                            <p>{data.description}</p>
                        </div>
                    );
                    })}
            </div>
        </div>
    );
};


export default BonsPlans;