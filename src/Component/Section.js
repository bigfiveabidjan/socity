import React from "react";
import '../App.css';
import {section} from '../data';

function Section() {
    return (
        <div className="component-section">
            {section.map((data, key) => {
            return (
                // eslint-disable-next-line
                <div className={"section "+ data.position} key={key}>
                    <div className="left">
                        <img src={data.image} alt=""/>
                    </div>
                    <div className="right">
                        <div className="zone-description">
                            <img src={data.favicon} alt=""/>
                            <h2>
                                {data.titre1}<br/>{data.titre2}
                            </h2>
                            <div className="separation"></div>
                            <p>{data.description}</p>
                        </div>
                    </div>
                </div>
            );
            })}

        </div>
    );
};


export default Section;