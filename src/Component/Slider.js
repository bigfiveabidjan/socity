import React from "react";
import '../App.css';
import leftImg from '../img/sliderimg.png';
import rightImg from '../img/socityimg.png';
import {slider} from '../data';
import BouttonTelechargement from './BouttonTelechargement';

function Slider() {
    return (
        <div className="component-slider">
            <div className="section">
                <div className="left">
                    <img src={leftImg} alt="socity"/>
                </div>
                <div className="right">
                    <img src={rightImg} alt="socity app"/>
                    <h1>
                        <strong>{slider.titre1}</strong>
                        {slider.titre2}
                    </h1>
                    <h2>{slider.description}</h2>
                    <BouttonTelechargement />
                </div>
            </div>

        </div>
    );
};


export default Slider;