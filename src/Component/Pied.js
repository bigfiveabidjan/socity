import React from "react";
import '../App.css';
import {footer} from '../data';
import BouttonTelechargement from './BouttonTelechargement';

function Pied() {
    return (
        <div className="component-pied">
            <div className="section">
                <div className="left">
                    <img src={footer.img} alt="socity"/>
                </div>
                <div className="right">
                    <div className="zone-description">
                        <img src={footer.favicon} alt=""/>
                        <h2>{footer.titre}</h2>
                        <div className="separation"></div>
                        <p>{footer.description}</p>
                        <BouttonTelechargement />
                    </div>
                </div>
            </div>

        </div>
    );
};


export default Pied;