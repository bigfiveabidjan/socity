import Entete from './Component/Entete.js'
import Slider from './Component/Slider';
import Section from './Component/Section';
import './App.css';
import BonsPlans from './Component/BonsPlans.js';
import Pied from './Component/Pied.js';

function App() {
  return (
    <div className="App">
      <Entete />
      <Slider />
      <Section />
      <BonsPlans />
      <Pied />
    </div>
  );
}

export default App;
