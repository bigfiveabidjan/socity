export const enTete = {
    site_url : "https://socity.ci/",
    logo_url: "./img/logo.png",
    logo_alt: "Socity - La ville dans ma poche"
};

export const menu = [
    {
        text: "ABIDJAN ON DIT QUOI?",
        link: "https://app.socity.ci/"
    },
    {
        text: "Se connecter",
        link: "https://dashboard.v2.socity.ci/"
    },
    {
        text: "Assistance",
        link: "https://socity.ci/assistance/"
    },
    {
        text: "Contact",
        link: "https://socity.ci/contact/"
    },
    {
        text: "Facebook",
        link: "https://www.facebook.com/mysocity/"
    }
];

export const slider = {
    titre1: "L’application 100% gratuite!",
    titre2: "qui met Abidjan dans votre poche !",
    description: "Retrouvez toutes les infos et itinéraires sur les meilleurs bars, maquis, restaurants, hôtels et événements d’Abidjan."
};

export const appLink = [
    {
        text: "Téléchargez sur Android",
        favicon: 'https://socity.ci/img/android.png',
        link: "https://play.google.com/store/apps/details?id=com.socity"
    },
    {
        text: "Téléchargez sur iOS",
        favicon: "https://socity.ci/img/apple.png",
        link: "https://itunes.apple.com/us/app/socity-abidjan/id1369173826?ls=1&mt=8"
    }
];

export const section = [
    {
        position: "gauche",
        image: "https://socity.ci/wp-content/uploads/2019/07/Groupe-7_3833ef002573220cda5188b0cbe4d2e5.png",
        favicon: "https://socity.ci/wp-content/uploads/2019/07/Group-3ss.png",
        titre1: "Composez votre soirée et",
        titre2: "ne manquez aucun événement.",
        description: "Organisez rapidement les différentes étapes de votre soirée grâce aux catégories pré-établies : maquis, restaurants, bars, discothèque… Sans oublier les principaux événements d’Abidjan !"
    },
    {
        position: "droite",
        image: "https://socity.ci/wp-content/uploads/2019/07/Groupe-7-1.png",
        favicon: "https://socity.ci/wp-content/uploads/2019/07/drillon.png",
        titre1: "Découvrez de nouveaux lieux et",
        titre2: "laissez-vous guider.",
        description: "Retrouvez toutes les infos pratiques des établissements les plus en vue d’Abidjan. Obtenez en temps réel l’itinéraire le plus court pour rejoindre votre établissement préféré."
    },
    {
        position: "gauche",
        image: "https://socity.ci/wp-content/uploads/2019/06/app-mobile-kachel3.png",
        favicon: "https://socity.ci/wp-content/uploads/2019/07/Group-3s.png",
        titre1: "Personnalisez votre expérience.",
        titre2: "",
        description: "Enregistrez vos lieux et événements préférés dans vos Favoris et partagez-les avec vos amis. Recevez des notifications pour rester informé de tous les bons plans, soirées, promotions et événements de vos établissements favoris."
    }
];

export const bonsPlans = [
    {
        class: "sortir",
        favicon: "https://socity.ci/wp-content/uploads/2019/07/Group-2@2x.png",
        titre: "Où sortir",
        description: "Bars, restaurants, maquis, , cafés, caves, discos, lounge…"
    },
    {
        class: "dormir",
        favicon: "https://socity.ci/wp-content/uploads/2019/07/Group-5@2x.png",
        titre: "Où dormir",
        description: "Hôtels,chambres à louer, maisons à louer…"
    },
    {
        class: "acheter",
        favicon: "https://socity.ci/wp-content/uploads/2019/07/Group-4@2x.png",
        titre: "Où acheter",
        description: "Distributeurs, stations essences, supermarchés…"
    },
    {
        class: "evenements",
        favicon: "https://socity.ci/wp-content/uploads/2019/07/Group-6@2x.png",
        titre: "Evenements",
        description: "Concerts, festivals, lancements de produits"
    },
    {
        class: "actualites",
        favicon: "https://socity.ci/wp-content/uploads/2019/07/Logo-Solibra-Blc@2x.png",
        titre: "Actualités",
        description: "les dernières news de Solibra"
    }
];

export const footer = {
    img: "https://socity.ci/wp-content/uploads/2019/06/tels-en-bas.png",
    favicon: "https://socity.ci/wp-content/uploads/2019/10/Group-6qsq.png",
    titre: "Téléchargez SoCITY",
    description: "Et mettez Abidjan dans votre poche"
}